# SomeThings documents #

This is the main project for document all the related staff about SomeThings...


There are different related projects to SomeThings environment.

* ST Engines
	- The 'ST Engines' is the part of the 'ST system' that defines the interactions with real world using specific devices like sensors or actuators.
	- The 'ST Engines' are used by 'ST Nodes' and managed by 'ST Server' using 'ST.Network'.
	
	Located at folder "ST.Engines"
	
* ST Network
	- The ST Network is the part of the 'ST system' that defines how the information data is managed.
	- The definition of 'data channels' and 'binds' provide customizable net topologies that could change in real time.
	- The ST Network also is used for interchange data and control messages between 'ST Nodes' and 'ST Server'.
	
	Located at folder "ST.Network"
	
* ST Node
	- 'ST Node' is the part of the 'ST system' where 'ST Engines' are connected.
	- Is remotely managed by the 'ST Server' using control messages over 'ST Network'
	
	Located at folder "ST.Node"

* ST Server
	- The ST Server is the part of the 'ST system' where 'ST Nodes' are connected.
	- Use 'ST Network' for provide control messages to 'ST Nodes'.
	- Could be administrated by HTTP/REST-JSON service.
	
	Located at folder "ST.Server"

* ST for Browser
	- The ST for Browser is a ST library for browsers...
	- Provides access to ST API.
	
	Located at folder "ST.forBrowser"

* ST Console App
	- The ST Console Application is an HTML5 application for manage ST Systems...
	- Uses the ST API.
	
	Located at folder "ST.ConsoleApp"